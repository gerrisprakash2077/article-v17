import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import './Navbar.css'
export default class Navbar extends Component {
    render() {
        return (
            <div className='bg-white float-left h-screen'>
                <nav className=' w-56 m-2'>
                    <NavLink to='/' exact>
                        <div className='p-2 hover:bg-slate-100 inline-block w-full'>

                            <svg className='w-7 inline-block' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
                                <path d="M575.8 255.5c0 18-15 32.1-32 32.1h-32l.7 160.2c0 2.7-.2 5.4-.5 8.1V472c0 22.1-17.9 40-40 40H456c-1.1 0-2.2 0-3.3-.1c-1.4 .1-2.8 .1-4.2 .1H416 392c-22.1 0-40-17.9-40-40V448 384c0-17.7-14.3-32-32-32H256c-17.7 0-32 14.3-32 32v64 24c0 22.1-17.9 40-40 40H160 128.1c-1.5 0-3-.1-4.5-.2c-1.2 .1-2.4 .2-3.6 .2H104c-22.1 0-40-17.9-40-40V360c0-.9 0-1.9 .1-2.8V287.6H32c-18 0-32-14-32-32.1c0-9 3-17 10-24L266.4 8c7-7 15-8 22-8s15 2 21 7L564.8 231.5c8 7 12 15 11 24z" />
                            </svg>

                            <p className='inline-block ml-3'>

                                Home
                            </p>

                        </div>
                    </NavLink>
                    <NavLink to='/Articles'>
                        <div className='p-2 hover:bg-slate-100 w-full'>
                            <svg className='w-7 inline-block mr=5' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"> <path d="M253.5 51.7C248.6 39.8 236.9 32 224 32s-24.6 7.8-29.5 19.7l-120 288-40 96c-6.8 16.3 .9 35 17.2 41.8s35-.9 41.8-17.2L125.3 384H322.7l31.8 76.3c6.8 16.3 25.5 24 41.8 17.2s24-25.5 17.2-41.8l-40-96-120-288zM296 320H152l72-172.8L296 320z" /></svg>
                            <p className='inline-block ml-3'>

                                Articles
                            </p>

                        </div>
                    </NavLink>
                </nav>
            </div>
        )
    }
}
